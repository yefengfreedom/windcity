# coding=utf-8
from django.conf.urls import patterns, include, url

from django.contrib import admin
from django.conf.urls.static import static
import os
from django.conf import settings

admin.autodiscover()

media_root = os.path.join(settings.BASE_DIR, 'media')

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'windcity.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'main.api.shopList'),
    url(r'^api/v1/shopList/$', 'main.api.shopList'),
    url(r'^api/v1/shopDetail/$', 'main.api.shopDetail'),
    url(r'^api/v1/submitOrder/$', 'main.api.submitOrder'),
    url(r'^api/v1/orderList/$', 'main.api.orderList'),
    url(r'^api/v1/commentOrder/$', 'main.api.commentOrder'),
    url(r'^api/v1/feedback/$', 'main.api.feedback'),
) + static('/media/', document_root=media_root)