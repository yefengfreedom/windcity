# coding=utf-8
from django.db import models

# Create your models here.
class Shop(models.Model):
    name = models.CharField(max_length=30, default='', unique=True)
    logo_url = models.URLField(blank=True, default='')
    logo_key = models.CharField(max_length=100, default='')
    intro = models.CharField(max_length=200, default='')
    status = models.IntegerField(default=0)
    readMe = models.CharField(max_length=4000, default='')
    price = models.CharField(max_length=40, default='')
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)
    sort = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('sort', 'status', 'id',)


class ShopWares(models.Model):
    shop_id = models.IntegerField(default=0)
    name = models.CharField(max_length=100, default='')
    price = models.DecimalField(max_digits=10, decimal_places=1, blank=True, default=0.0)
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)
    sort = models.IntegerField(default=0)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('shop_id', 'sort', 'id',)


class ShopGallery(models.Model):
    shop_id = models.IntegerField(default=0)
    pic_url = models.URLField(blank=True, default='')
    pic_key = models.CharField(max_length=100, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)
    sort = models.IntegerField(default=0)

    def __unicode__(self):
        return self.pic_key

    class Meta:
        ordering = ('shop_id', 'sort', 'id',)


class ShopComment(models.Model):
    shop_id = models.IntegerField(default=0)
    order_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    name = models.CharField(max_length=30, default='')
    content = models.CharField(max_length=1000, default='')
    score = models.IntegerField(default=0)
    show_order = models.IntegerField(default=0)
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('ctime',)


class Order(models.Model):
    shop_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    ware_id = models.IntegerField(default=0)
    ware_num = models.IntegerField(default=0)
    remark = models.CharField(max_length=1000, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('ctime',)


class UserUploadPhoto(models.Model):
    order_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    pic_url = models.URLField(blank=True, default='')
    pic_key = models.CharField(max_length=100, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.pic_key

    class Meta:
        ordering = ('ctime',)


class OrderPhoto(models.Model):
    order_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    pic_url = models.URLField(blank=True, default='')
    pic_key = models.CharField(max_length=100, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.pic_key

    class Meta:
        ordering = ('ctime',)


class User(models.Model):
    user_name = models.CharField(max_length=50, default='', unique=True)
    password = models.CharField(max_length=50, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)
    sex = models.IntegerField(default=0)
    phone = models.CharField(max_length=20, default='')
    email = models.EmailField(default='')
    qq = models.CharField(max_length=20, default='')
    wechat = models.CharField(max_length=20, default='')
    device_id = models.CharField(max_length=200, default='')
    device_type = models.IntegerField(default='')
    avatar = models.URLField(blank=True, default='')
    source = models.CharField(max_length=50, default='')
    source_id = models.IntegerField(default=0)

    def __unicode__(self):
        return self.user_id


class UserToken(models.Model):
    user_id = models.IntegerField(default=0)
    token = models.CharField(max_length=2000, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.token


class Feedback(models.Model):
    user_id = models.IntegerField(default=0)
    user_name = models.CharField(max_length=50, default='', unique=True)
    content = models.CharField(max_length=4000, default='')
    contact = models.CharField(max_length=100, default='')
    status = models.IntegerField(default=0)
    ctime = models.DateTimeField(auto_now_add=True)
    utime = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return self.content

    class Meta:
        ordering = ('ctime',)


