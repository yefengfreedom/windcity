# coding=utf-8

__author__ = 'night'

import json

from django.http import HttpResponse
from rest_framework.renderers import JSONRenderer
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize

from models import Shop, ShopWares, ShopGallery, ShopComment, OrderPhoto, Order, UserUploadPhoto, Feedback


class JSONResponse(HttpResponse):
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)


def returnResponse(code=0, msg='', data=''):
    responseJson = dict()
    responseJson['code'] = code
    responseJson['msg'] = msg
    responseJson['data'] = data
    return JSONResponse(responseJson)


@csrf_exempt
def shopList(request):
    shops = Shop.objects.all()
    shopList = []
    for shop in shops:
        shopWares = ShopWares.objects.filter(shop_id=shop.id).order_by("price")
        if shopWares.count() > 1:
            price = u'￥' + str(shopWares[0].price) + u'~￥' + str(shopWares[shopWares.count() - 1].price)
        elif shopWares.count() == 1:
            price = u'￥' + str(shopWares[0].price)
        else:
            price = ''
        responseShop = dict()
        responseShop['id'] = shop.id
        responseShop['name'] = shop.name
        responseShop['logo_key'] = shop.logo_key
        responseShop['logo_url'] = shop.logo_url
        responseShop['intro'] = shop.intro
        responseShop['price'] = price
        responseShop['status'] = shop.status
        shopList.append(responseShop)
    return returnResponse(0, '', shopList)


@csrf_exempt
def shopDetail(request):
    shopId = request.REQUEST.get('id')
    shop = Shop.objects.get(id=shopId)
    response_data = dict()
    response_data['id'] = shop.id
    response_data['name'] = shop.name
    response_data['logo_key'] = shop.logo_key
    response_data['logo_url'] = shop.logo_url
    response_data['intro'] = shop.intro
    shopWares = ShopWares.objects.filter(shop_id=shopId).order_by("price")
    if shopWares.count() > 1:
        price = u'￥' + str(shopWares[0].price) + u'~￥' + str(shopWares[shopWares.count() - 1].price)
    elif shopWares.count() == 1:
        price = u'￥' + str(shopWares[0].price)
    else:
        price = ''
    response_data['price'] = price
    wares = []
    for shopWare in shopWares:
        ware = dict()
        ware['id'] = shopWare.id
        ware['name'] = shopWare.name
        ware['price'] = str(shopWare.price)
        wares.append(ware)
    response_data['ware'] = wares
    response_data['status'] = shop.status
    response_data['readMe'] = shop.readMe
    gallery = []
    galleries = ShopGallery.objects.filter(shop_id=shopId)
    for g in galleries:
        gallery.append(g.pic_key)
    response_data['gallery'] = gallery
    commentNum = ShopComment.objects.count()
    response_data['commentNum'] = commentNum
    comments = []
    shopComments = ShopComment.objects.all()
    totalScore = 0
    for shopComment in shopComments:
        totalScore += shopComment.score
        c = dict()
        c['id'] = shopComment.id
        c['name'] = shopComment.name
        c['time'] = str(shopComment.ctime)
        c['content'] = shopComment.content
        c['score'] = shopComment.score
        orderPhotos = OrderPhoto.objects.filter(order_id=shopComment.order_id)
        pics = []
        for orderPhoto in orderPhotos:
            pics.append(orderPhoto.pic_key)
        c['pics'] = pics
        comments.append(c)
    response_data['comments'] = comments
    response_data['score'] = totalScore / commentNum
    print 302
    return returnResponse(0, '', response_data)


@csrf_exempt
def orderList(request):
    userId = request.REQUEST.get('userId')
    response_data = []
    orders = Order.objects.filter(user_id=userId)
    for order in orders:
        orderDetail = dict()
        orderDetail['id'] = order.id
        orderDetail['shop'] = Shop.objects.get(id=order.shop_id).name
        orderDetail['shopId'] = order.shop_id
        orderDetail['orderTime'] = str(order.ctime)
        orderDetail['status'] = order.status
        orderDetail['remark'] = order.remark
        originalPhotos = UserUploadPhoto.objects.filter(order_id=order.id)
        originalPhotoList = []
        for originalPhoto in originalPhotos:
            originalPhotoList.append(originalPhoto.pic_key)
        orderDetail['originalPhotos'] = originalPhotoList
        newPhotos = OrderPhoto.objects.filter(order_id=order.id)
        newPhotoList = []
        for newPhoto in newPhotos:
            newPhotoList.append(newPhoto.pic_key)
        orderDetail['newPhotos'] = newPhotoList
        isCommented = 0
        try:
            shopComment = ShopComment.objects.get(order_id=order.id)
            comment = dict()
            comment['id'] = shopComment.id
            comment['score'] = shopComment.score
            comment['time'] = str(shopComment.ctime)
            comment['content'] = shopComment.content
            if shopComment.show_order == 0:
                comment['photos'] = newPhotoList
            else:
                comment['photos'] = []
            orderDetail['comment'] = comment
        except ShopComment.DoesNotExist:
            isCommented = 1
            orderDetail['comment'] = dict()
        orderDetail['isCommented'] = isCommented
        response_data.append(orderDetail)
    return returnResponse(0, '', response_data)


@csrf_exempt
def commentOrder(request):
    userId = request.REQUEST.get('userId')
    username = request.REQUEST.get('username')
    shopId = request.REQUEST.get('shopId')
    orderId = request.REQUEST.get('id')
    score = request.REQUEST.get('score')
    content = request.REQUEST.get('content')
    showPhoto = request.REQUEST.get('showPhoto')
    ShopComment.objects.get_or_create(shop_id=shopId, order_id=orderId, user_id=userId, name=username, content=content,
                                      score=score, show_order=showPhoto)
    return returnResponse(0, '评论成功', '')


@csrf_exempt
def feedback(request):
    userId = request.REQUEST.get('userId')
    username = request.REQUEST.get('username')
    content = request.REQUEST.get('content')
    contact = request.REQUEST.get('contact')
    Feedback.objects.get_or_create(user_id=userId, user_name=username, content=content, contact=contact)
    return returnResponse(0, '意见反馈成功', '')


@csrf_exempt
def submitOrder(request):
    shopId = request.REQUEST.get('shopId')
    userId = request.REQUEST.get('userId')
    wareId = request.REQUEST.get('wareId')
    wareNum = request.REQUEST.get('wareNum')
    remark = request.REQUEST.get('remark')
    pic = request.REQUEST.get('pic')
    order = Order()
    order.shop_id = shopId
    order.user_id = userId
    order.ware_id = wareId
    order.ware_num = wareNum
    order.remark = remark
    order.save()
    orderId = order.id
    picJsonArray = json.loads(pic)
    for picJson in picJsonArray:
        uup = UserUploadPhoto()
        uup.order_id = orderId
        uup.user_id = userId
        uup.pic_key = picJson.get('pic_key')
        uup.pic_url = picJson.get('pic_url')
        uup.save()
    return returnResponse(0, '订单提交成功', '')