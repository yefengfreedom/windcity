# coding=utf-8
__author__ = 'night'
from rest_framework import serializers
from api_model import ResponseShop


class ResponseShopSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = ResponseShop
        fields = ('id', 'name', 'logo', 'intro', 'price', 'status')